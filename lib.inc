section .text

%define SYS_EXIT 60
%define SYS_WRITE 1
%define SYS_READ 0

%define STDIN 0
%define STDOUT 1

%define WHITESP_1 0x20
%define WHITESP_2 0x9
%define WHITESP_3 0xA

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, SYS_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rsi, rsi
    mov rax, rdi
    .iterate:                       ; over string
        mov sil, [rax]              ; get current char code
        test sil, sil               ; check if now at null terminator
        jz .stop            		; stop if string ended
        inc rax             		; otherwise increase rax by 1
        jmp .iterate        		; and keep iterating
    .stop:
        sub rax, rdi
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi            ; save string pointer
    call string_length  ; get string length
    mov rdx, rax        ; rdx = string length
    pop rsi             ; rsi = string pointer
    mov rdi, STDOUT     ; rdi = file descriptor
    mov rax, SYS_WRITE  ; rax = sys_write signal number
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi            ; save to stack
    mov rsi, rsp        ; rsi = string pointer
    mov rdx, 1          ; rdx = string length
    mov rdi, STDOUT     ; rdi = file descriptor
    mov rax, SYS_WRITE  ; sys_write
    syscall
    pop rdi             ; restore stack
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, '\n'       ; newline char code

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0          ; compare with zero
    jge print_uint      ; if negative
    neg rdi             ; negate it
    push rdi            ; save to stack
    mov rdi, '-'        ; rdi = '-'
    call print_char     ; print minus
    pop rdi             ; restore number from stack

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    dec rsp
    mov byte[rsp], 0

    xor rcx, rcx
    mov r9, 10

    .iterate:
        xor rdx, rdx
        div r9
        add rdx, '0'
        dec rsp
        mov [rsp], dl
        inc rcx

    test rax, rax
    jne .iterate

    mov rdi, rsp
    push rcx
    call print_string
    pop rcx
    inc rsp
    add rsp, rcx
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax        ; result = 0
    xor dx, dx          ; chars
    .iterate:
        mov dh, [rdi]   ; get char from first string
        mov dl, [rsi]   ; get char from second string
        cmp dh, dl      ; compare them
        jne .false      ; if equal
        test dh, dh     ;   check if they're null terminator
        jz .true        ;   if they're not
        inc rdi         ;       move on to next pair of chars and iterate again
        inc rsi         ;   else return true
        jmp .iterate    ; else return false
    .true:
        inc rax         ; result = 1
        ret
    .false:
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rax, SYS_READ   ; rax = sys_read signal number
    push rax            ; push zero to stack
    mov rdi, STDIN      ; rdi = file descriptor
    mov rsi, rsp        ; rsi = buffer address (stack top)
    mov rdx, 1          ; rdx = byte count
    syscall             ; rax = number of read bytes
	pop rax
	ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    xor rcx, rcx
    .iterate:
        push rdi
        push rsi
        push rcx
        call read_char
        pop rcx
        pop rsi
        pop rdi
        cmp rax, WHITESP_1
        je .test_leading_whitespace
        cmp rax, WHITESP_2
        je .test_leading_whitespace
        cmp rax, WHITESP_3
        je .test_leading_whitespace
        test rax, rax
        jz .end
        mov byte[rdi+rcx], al
        inc rcx
        cmp rcx, rsi
        jl .iterate
        mov rax, 0
        ret

    .test_leading_whitespace:
        test rcx, rcx
        jz .iterate

    .end:
        mov byte[rdi+rcx], 0
        mov rdx, rcx
        mov rax, rdi
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    mov r10b, [rdi]

    cmp r10b, '-'
    je .negative

    cmp r10b, '+'
    je .positive

	jmp parse_uint

    .positive:
        inc rdi
        call parse_uint
        inc rdx
        ret

    .negative:
        inc rdi
        call parse_uint
        inc rdx
        neg rax
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor r10, r10
    mov r11, 10

    .iterate:
        mov r10b, byte[rdi+rdx]

        cmp r10b, '0'
        jl .return

        cmp r10b, '9'
        jg .return

        push rdx

        mul r11

        pop rdx

        sub r10b, '0'

        add rax, r10

        inc rdx
        jmp .iterate

        .return:
            ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax

    .iterate:
        mov bl, byte[rdi+rax]
        mov byte[rsi+rax], bl
        dec rdx
        js .error
        inc rax
        cmp byte[rdi + rax - 1], 0
        jne .iterate
		ret

        .error:
            mov rax, 0
			ret
